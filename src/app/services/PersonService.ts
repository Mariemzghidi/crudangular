import { Injectable } from '@angular/core';
import { Person } from '../model/person';


@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private persons: Person[] = [
    { id: 1, firstName: 'John', lastName: 'Doe', email: 'john@example.com' },
    { id: 2, firstName: 'Jane', lastName: 'Smith', email: 'jane@example.com' },
    { id: 3, firstName: 'Aylan', lastName: 'Sam', email: 'aylan@example.com' },
    { id: 4, firstName: 'Alex', lastName: 'peter', email: 'alex@example.com' },
  ];


  getAllPersons(): Person[] {
    return this.persons;
  }

  getPersonById(id: number): Person | undefined {
    return this.persons.find(person => person.id === id);
  }

  addPerson(person: Person): void {
    let incrementingValue = 4;

  function generateIncrementingId() {
  return incrementingValue++;
}
    person.id =generateIncrementingId() ;
    this.persons.push(person);
  }

  updatePerson(person: Person): void {
    const index = this.persons.findIndex(p => p.id === person.id);

    if (index !== -1) {
      this.persons[index] = person;
    }

  }

  deletePerson(id: number): void {
    this.persons = this.persons.filter(person => person.id !== id);
  }
}
