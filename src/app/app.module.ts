import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { AppComponent } from './app.component';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { UserDetailsComponent } from './user-menu/user-details/user-details.component';
import { UserEditComponent } from './user-menu/user-edit/user-edit.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { UserDeleteComponent } from './user-menu/user-delete/user-delete.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { UserAddComponent } from './user-menu/user-add/user-add.component';
@NgModule({
  declarations: [
    AppComponent,
    UserMenuComponent,
    UserDetailsComponent,
    UserEditComponent,
    UserDeleteComponent,
    UserAddComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
