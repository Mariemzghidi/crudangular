export class Person {
  id: number | undefined;
  firstName: string | undefined;
  lastName: string| undefined;
  email: string | undefined;
}
