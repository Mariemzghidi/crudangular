import { Component, Inject, OnInit } from '@angular/core';
import { UserMenuComponent } from '../user-menu.component';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class  UserDetailsComponent implements OnInit {
obj;
constructor(public dialogRef: MatDialogRef<UserMenuComponent>,
  @Inject(MAT_DIALOG_DATA)public data : any) {
    this.obj= data ;
    console.log(data);
  }

  ngOnInit(): void {
  }

}
