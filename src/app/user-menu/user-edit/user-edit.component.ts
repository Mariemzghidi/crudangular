import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
  Inject,
} from "@angular/core";
import { Subject } from 'rxjs';
import {
  FormBuilder,
  FormControl,
  FormGroupDirective,
  FormGroup,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UserMenuComponent } from "../user-menu.component";
import { Person } from "src/app/model/person";
import { PersonService } from "src/app/services/PersonService";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  @ViewChild(FormGroupDirective)
  formDirective!: FormGroupDirective;
  obj ;
  form!: FormGroup;
  constructor(private personService: PersonService, private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<UserMenuComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
      this.obj = data;
    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id:[this.obj.id],
      firstName: [this.obj.firstName],
      lastName: [this.obj.lastName],
      email:[this.obj.email],

    });
  }

  update() {

    this.personService.updatePerson(this.form.value);
    this.dialogRef.close('updated');


  }
  reset() {
    this.form.reset();
    this.formDirective.resetForm();}

}
