import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Person } from 'src/app/model/person';
import { PersonService } from 'src/app/services/PersonService';
import { UserMenuComponent } from '../user-menu.component';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.css']
})
export class UserDeleteComponent implements OnInit {
 obj ;
 constructor(private personService: PersonService,
  public dialogRef: MatDialogRef<UserMenuComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.obj = data;
  }

  ngOnInit(): void {
  }
  delete(){
    this.personService.deletePerson(this.obj.id);
    this.dialogRef.close('updated');
  }

}
