import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Person } from '../model/person';
import { PersonService } from '../services/PersonService';
import { MatDialog } from "@angular/material/dialog";
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserDeleteComponent } from './user-delete/user-delete.component';
import { UserAddComponent } from './user-add/user-add.component';
import { MatTableDataSource } from '@angular/material/table';



@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css'],
})
export class UserMenuComponent implements OnInit {
  displayedColumns: string[] = [ 'firstName', 'lastName', 'email','action'];
  dataSource  :  Person[] = [] ;


  constructor(private personService: PersonService,
    public dialog: MatDialog, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.getListPerson();

  }
openEdit(obj:Person){
  console.log("obj",obj)
  this.dialog
      .open(UserEditComponent, {
        width: "40%",

        data: obj,
      })
      .afterClosed()
      .subscribe((result) => {

        if (result === 'updated') {
          this.getListPerson();

          console.log("men fc",this.dataSource )

        }


      });
 }
opendetails(obj: Person){
   console.log("obj",obj)
   console.log("opeeen")
  const dialogRef = this.dialog.open(UserDetailsComponent, {
    width: "50%",
    data:
     obj
    ,
  });}
opendelete(obj:Person){
  console.log("obj",obj)
  this.dialog
      .open(UserDeleteComponent, {
        width: "40%",

        data: obj,
      })
      .afterClosed()
      .subscribe((result) => {

          if (result === 'updated') {
            this.getListPerson();



        }
      });

}
getListPerson(){
  this.dataSource = this.personService.getAllPersons();
 this.cdr.detectChanges();
}
addUser(){

const dialogRef = this.dialog.open(UserAddComponent, {
 width: "50%",


});}
}
