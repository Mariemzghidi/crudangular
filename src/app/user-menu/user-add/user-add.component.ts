import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroupDirective, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PersonService } from 'src/app/services/PersonService';
import { UserMenuComponent } from '../user-menu.component';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
  @ViewChild(FormGroupDirective)
  formDirective!: FormGroupDirective;

  form!: FormGroup;
  constructor(private personService: PersonService, private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<UserMenuComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {

    }

  ngOnInit(): void {
    this.form = this.formBuilder.group({

      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      email:[null, Validators.required],

    });
  }

  add() {

    this.personService.addPerson(this.form.value);
    this.dialogRef.close('updated');


  }
  reset() {
    this.form.reset();
    this.formDirective.resetForm();}


}
